# DeliverMe App:

# Requirements:
1. Xcode 10.3 or later
2. Target iOS 11.0 or later

# To run the project you need to install Pod:
1. Open Terminal
2. Go to project folder
3. run "pod install" command

# Programming Language:
Swift 5.0

# Design Pattern:
MVVM
Singleton

![Scheme](SampleImages/architecture.png)

# App Flow:
1. Get list from server if internet available otherwise from local db.
2. Show list of deliveries on Home screen.
3. After select any delivery record, will move to Delivery Detail screen on which location of delivery will show on map.

# Assumptions:
1. If 0 records get from server then will assume there is no more data and will not show loader for next page.
2. If internet not available then will use local db to get list.
3. App is designed for iPhone portrait mode.

# Caching:
1. For deliveries list cache, using core data
2. For delivery image cache, using SDWebImage


# Libraries Used via Pod

Alamofire
SwiftLint
SnapKit
SDWebImage
ReachabilitySwift
MBProgressHUD
Fabric
Crashlytics

# To use Swiftlint:
1. Go to project-> Target -> Build phases
2. Add 'New Run Script Phase'
3. Add following:   "${PODS_ROOT}/SwiftLint/swiftlint"
4. You can specify rules for swiftlint in '.swiftlint.ymlt' file under project root folder.
5. Build the project to see if any other error remain.

# Improvements:
1. Test case for Alamofire .
2. UI test cases.


# Screenshots
![Scheme](SampleImages/deliveryList.png)
![Scheme](SampleImages/deliveryDetails.png)
