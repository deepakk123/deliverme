//
//  Constants.swift
//  DeliverMe
//
//  Created by Deepak kumar on 01/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation

struct ApiUrl {
    static let baseUrl = "https://mock-api-mobile.dev.lalamove.com"
}

struct RequestKeys {
    static let deliveryEndPoint = "/deliveries"

    static let offset    = "offset"
    static let limit     = "limit"
}

struct ResponseKeys {
    static let id           = "id"
    static let description  = "description"
    static let imageUrl     = "imageUrl"
    static let location     = "location"
    static let lat          = "lat"
    static let lng          = "lng"
    static let address      = "address"
}

struct StringConstants {
    static let deliveryListTitle = "Things To Deliver"
    static let deliveryDetailsTitle = "Delivery Details"
    static let okTitle = "Ok"
    static let noDataErrorMsg = "Please check your internet connection and pull down to refresh the list."
}

struct CoreDataEntities {
    static let delivery = "Delivery"
    static let location = "Location"
}
