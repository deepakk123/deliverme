//
//  DeliveryViewCell.swift
//  DeliverMe
//
//  Created by Deepak kumar on 01/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
import SDWebImage
import SnapKit

class DeliveryViewCell: UITableViewCell {

    var deliveryImgView: UIImageView!
    var deliveryLabel: UILabel!

    //constraints
    let imgTopConst = 10
    let imgLeftConst = 0
    let imgRightConst = -20
    let imgWidthHeightConst = 60
    let descLblTopConst = 10
    let descLblRightConst = -10
    let descLblBottomConst = -10
    let descLblHeightConst = 60
    let imgCornerRadius: CGFloat = 10.0

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupUI()
        setConstraints()
    }

    func setupUI() {
        deliveryImgView = UIImageView.init()
        deliveryImgView.translatesAutoresizingMaskIntoConstraints = false
        deliveryImgView.clipsToBounds = true
        deliveryImgView.contentMode = .scaleAspectFill
        self.contentView.addSubview(deliveryImgView)
        deliveryImgView.layer.cornerRadius = imgCornerRadius

        deliveryLabel = UILabel.init()
        deliveryLabel.numberOfLines = 0
        self.contentView.addSubview(deliveryLabel)
    }

    func setConstraints() {
        deliveryImgView.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp_topMargin).offset(imgTopConst)
            make.left.equalTo(contentView.snp_leftMargin).offset(imgLeftConst)
            make.right.equalTo(deliveryLabel.snp_leftMargin).offset(imgRightConst)
            make.width.height.equalTo(imgWidthHeightConst)
        }

        deliveryLabel.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp_topMargin).offset(descLblTopConst)
            make.right.equalTo(contentView.snp_rightMargin).offset(descLblRightConst)
            make.bottom.equalTo(contentView.snp_bottomMargin).offset(descLblBottomConst)
            make.height.greaterThanOrEqualTo(descLblHeightConst)
        }
    }

    func configureCellData(with deliveryViewModel: DeliveryListViewModelProtocol, at index: Int) {
        if index < deliveryViewModel.getDeliveiesCount() {
            self.deliveryImgView.sd_setImage(with: deliveryViewModel.getDeliveryImageUrl(forIndex: index), completed: nil)
            self.deliveryLabel?.text = deliveryViewModel.getDeliveryText(forIndex: index)
        }
    }

}
