//
//  DeliveryDetailsViewController.swift
//  DeliverMe
//
//  Created by Deepak kumar on 30/09/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
import MapKit

class DeliveryDetailsViewController: UIViewController {
    var mapView: MKMapView!
    var deliveryDetailsView: DeliveryDetailView!
    var deliveryPin: CustomMarker!
    let visibilityDistnace = 1000.0

    let annotationIdentifier = "marker"
    //Constraits
    private
    let mapLeftConstraint = -20
    let mapTopConstraint = 0
    let mapRightConstraint = 20
    let mapBottomConstraint = 0
    let detailViewLeftConstraint = -20
    let detailViewRightConstraint = 20
    let detailViewBottomConstraint = 0
    let detailViewHeightConstraint = 130

    var mapViewModel: DeliveryDetailViewModel!

    init(detailViewModel: DeliveryDetailViewModel) {
        super.init(nibName: nil, bundle: nil)
        mapViewModel = detailViewModel
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        setConstraints()
    }

    func setupUI() {
        title = StringConstants.deliveryDetailsTitle
        view.backgroundColor = .white

        mapView = MKMapView()
        mapView.delegate = self
        mapView.showsUserLocation = true
        view.addSubview(mapView)

        deliveryDetailsView = DeliveryDetailView()
        view.addSubview(deliveryDetailsView)
        deliveryDetailsView.setImage(mapViewModel.deliveryModel.imageUrl ?? "", details: mapViewModel.getDeliveryText())

        let pinLocation: CLLocationCoordinate2D = mapViewModel.deliveryLocation
        deliveryPin = CustomMarker.init(withTitle: mapViewModel.deliveryModel.location?.address ?? "", coordinate: pinLocation)
        mapView.addAnnotation(deliveryPin)

        let viewRegion = MKCoordinateRegion(center: pinLocation, latitudinalMeters: visibilityDistnace, longitudinalMeters: visibilityDistnace)
        mapView.setRegion(viewRegion, animated: true)
    }

    func setConstraints() {
        mapView.snp.makeConstraints { (make) in
            make.leftMargin.equalTo(view.snp_leftMargin).offset(mapLeftConstraint)
            make.top.equalTo(view.snp_topMargin).offset(mapTopConstraint)
            make.rightMargin.equalTo(view.snp_rightMargin).offset(mapRightConstraint)
            make.bottom.equalTo(deliveryDetailsView.snp_topMargin).offset(mapBottomConstraint)
        }

        deliveryDetailsView.snp.makeConstraints { (make) in
            make.leftMargin.equalTo(view.snp_leftMargin).offset(detailViewLeftConstraint)
            make.rightMargin.equalTo(view.snp_rightMargin).offset(detailViewRightConstraint)
            make.bottomMargin.equalTo(view.snp_bottomMargin).offset(detailViewBottomConstraint)
            make.height.equalTo(detailViewHeightConstraint)
        }
    }
}

extension DeliveryDetailsViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? CustomMarker else { return nil }

        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            view.canShowCallout = true
            view.detailCalloutAccessoryView = UIView()
        }
        return view
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {

    }
}
