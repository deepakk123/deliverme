//
//  DeliveryListViewController.swift
//  DeliverMe
//
//  Created by Deepak kumar on 30/09/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
import MBProgressHUD

class DeliveryListViewController: UIViewController {

    var refreshControl = UIRefreshControl()
    var tableView: UITableView!
    var activityLoader: UIActivityIndicatorView!

    var deliverylistViewModel: DeliveryListViewModelProtocol = DeliveryListViewModel()

    //constants
    let cellIdentifier = "cell"
    let defaultRowHeight: CGFloat = 100.0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        setConstraints()
        setEventHandlers()

        MBProgressHUD.showAdded(to: self.view, animated: true)
        deliverylistViewModel.getDeliveryList(pullToRefresh: false)
    }

    func setupUI() {
        title = StringConstants.deliveryListTitle
        tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self

        tableView.register(DeliveryViewCell.self, forCellReuseIdentifier: cellIdentifier)
        view.addSubview(tableView)

        tableView.estimatedRowHeight = defaultRowHeight

        refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableView.refreshControl = refreshControl

        activityLoader = UIActivityIndicatorView(style: .whiteLarge)
        activityLoader.color = .gray
        tableView.tableFooterView = activityLoader
    }

    func setConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    func setEventHandlers() {
        deliverylistViewModel.refreshHandler = { [weak self] in
            guard let weakSelf = self else {
                return
            }

            weakSelf.removeLoader()
            weakSelf.tableView.reloadData()
        }

        deliverylistViewModel.failedHandler = {[weak self] (error) in
            self?.removeLoader()
            if let error = error {
                self?.showAlert(message: error)
            }
        }

        deliverylistViewModel.loadMoreHandler = { [weak self] in
            self?.showBottomLoader()
        }
    }

    @objc func refreshList() {
        deliverylistViewModel.reloadListWithRefresh()
    }

    func removeLoader() {
        MBProgressHUD.hide(for: view, animated: true)
        //If no internet and no local db, then will hide refresh control after a delay, otherwise will show forever.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            self?.refreshControl.endRefreshing()
        }

        hideBottomLoader()
    }

    func hideBottomLoader() {
        activityLoader.stopAnimating()
        tableView.tableFooterView = nil
    }

    func showBottomLoader() {
        tableView.tableFooterView = activityLoader
        activityLoader.startAnimating()
    }
}

extension DeliveryListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deliverylistViewModel.deliveries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let deliveryCellq = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? DeliveryViewCell
        guard let deliveryCell = deliveryCellq else {
            return UITableViewCell()
        }
        deliveryCell.configureCellData(with: deliverylistViewModel, at: indexPath.row)
        deliveryCell.selectionStyle = .none

        return deliveryCell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        deliverylistViewModel.checkForFetchNextPage(indexPath.row)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedDelivery = deliverylistViewModel.getItemAtIndex(indexPath.row) {
            let mapViewModel = DeliveryDetailViewModel(delivery: selectedDelivery)
            let deliveryDetailsVC = DeliveryDetailsViewController(detailViewModel: mapViewModel)
            navigationController?.pushViewController(deliveryDetailsVC, animated: true)
        }
    }

}
