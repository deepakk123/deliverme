//
//  CustomMarker.swift
//  DeliverMe
//
//  Created by Deepak kumar on 05/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
import MapKit

class CustomMarker: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String? = ""

    init(withTitle: String = "", coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
        self.title = withTitle
    }
}
