//
//  DeliveryDetailView.swift
//  DeliverMe
//
//  Created by Deepak kumar on 04/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
import SDWebImage
import SnapKit

class DeliveryDetailView: UIView {
    var deliveryImgView: UIImageView!
    var deliveryLabel: UILabel!
    let imgCornerRadius: CGFloat = 10.0

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
        setConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupUI() {
        deliveryImgView = UIImageView.init()
        deliveryImgView.translatesAutoresizingMaskIntoConstraints = false
        deliveryImgView.clipsToBounds = true
        deliveryImgView.contentMode = .scaleAspectFill
        self.addSubview(deliveryImgView)
        deliveryImgView.layer.cornerRadius = imgCornerRadius

        deliveryLabel = UILabel.init()
        deliveryLabel.numberOfLines = 0
        self.addSubview(deliveryLabel)

        self.backgroundColor = .white
    }

    func setConstraints() {
        deliveryImgView.snp.makeConstraints {[unowned self] (make) in
            make.top.equalTo(self.snp_topMargin).offset(10)
            make.left.equalTo(self.snp_leftMargin).offset(10)
            make.right.equalTo(self.deliveryLabel.snp_leftMargin).offset(-20)
            make.width.height.equalTo(60)
        }

        deliveryLabel.snp.makeConstraints {[unowned self] (make) in
            make.top.equalTo(self.snp_topMargin).offset(10)
            make.right.equalTo(self.snp_rightMargin).offset(-10)
            make.bottom.equalTo(self.snp_bottomMargin).offset(-10)
            make.height.greaterThanOrEqualTo(60)
        }
    }

    func setImage(_ image: String, details: String) {
        self.deliveryImgView.sd_setImage(with: URL(string: image), completed: nil)
        self.deliveryLabel.text = details
    }
}
