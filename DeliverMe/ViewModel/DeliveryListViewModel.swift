//
//  DeliveryListViewModel.swift
//  DeliverMe
//
//  Created by Deepak kumar on 30/09/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation

class DeliveryListViewModel: DeliveryListViewModelProtocol {

    var refreshHandler: CompletionClosure?
    var failedHandler: FailedClosure?
    var loadMoreHandler: LoadMoreClosure?

    var reachabilityManager: ReachabilityProtocol = ReachabilityManager.sharedInstance
    var dataManager: DataManagerProtocol = DataManager()
    var dbManager: DBManagerProtocol = DBManager.sharedInstance
    let listPerPage = 20

    var isApiLoading = false
    var deliveries: [DeliveryModel] = [] {
        didSet {
            refreshData()
        }
    }

    var shouldGetNextPage: Bool = true

    func refreshData() {
        refreshHandler?()
    }

    func getDeliveryList(pullToRefresh: Bool) {
        guard reachabilityManager.isNetworkReachable() || dbManager.isCacheAvailable() else {
            failedHandler?(StringConstants.noDataErrorMsg)
            return
        }

        if isApiLoading {       //If api request already placed, will return
            return
        }
        isApiLoading = true

        dataManager.getDeliveriesList(offset: pullToRefresh ? 0 : deliveries.count, limit: listPerPage, completed: { [weak self] newDeliveries in
            self?.isApiLoading = false

            guard let weakSelf = self, let newDeliveries = newDeliveries as? [DeliveryModel] else {
                return
            }

            weakSelf.shouldGetNextPage = true
            if newDeliveries.isEmpty { // when count is 0 it means no more data remained
                weakSelf.shouldGetNextPage = false
            }

            if pullToRefresh {
                weakSelf.deliveries = newDeliveries       //Reset the list to show latest records in case of refresh.
                return
            }

            weakSelf.deliveries.append(contentsOf: newDeliveries)
        }, failed: {[weak self] (error) in
            self?.isApiLoading = false
            self?.failedHandler?(error)
        })
    }

    func checkForFetchNextPage(_ index: Int) {
        if index == getDeliveiesCount() - 1 {
            getDeliveryListNextPage()
        }
    }

    func getDeliveryListNextPage() {
        if shouldGetNextPage {
            loadMoreHandler?()
            getDeliveryList(pullToRefresh: false)
        }
    }

    func reloadListWithRefresh() {
        getDeliveryList(pullToRefresh: true)
    }

    func getDeliveiesCount() -> Int {
        return deliveries.count
    }

    func getDeliveryText(forIndex index: Int) -> String {
        guard index < getDeliveiesCount() else {
            return ""
        }
        return getDescriptionText(delivery: deliveries[index])
    }

    func getDescriptionText(delivery: DeliveryModel) -> String {
        guard let desStr = delivery.description else {
            return ""
        }
        if let addr = delivery.location?.address {
            return desStr + " at " + addr
        }
        return desStr
    }

    func getDeliveryImageUrl(forIndex index: Int) -> URL? {
        if index < getDeliveiesCount(), let imgUrl = deliveries[index].imageUrl {
            return URL.init(string: imgUrl)
        }
        return nil
    }

    func getItemAtIndex(_ index: Int) -> DeliveryModel? {
        guard index < getDeliveiesCount() else {
            return nil
        }
        return deliveries[index]
    }
}
