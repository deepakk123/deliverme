//
//  DeliveryDetailViewModel.swift
//  DeliverMe
//
//  Created by Deepak kumar on 04/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
import CoreLocation

class DeliveryDetailViewModel: NSObject {
    var deliveryModel: DeliveryModel!
    var deliveryLocation: CLLocationCoordinate2D!

    init(delivery: DeliveryModel) {
        deliveryModel = delivery
        deliveryLocation = CLLocationCoordinate2D.init(latitude: deliveryModel.location?.lat ?? 0.0, longitude: delivery.location?.lng ?? 0.0)
    }

    // Model properties
    func getDeliveryText() -> String {
        guard let desStr = deliveryModel.description else {
            return ""
        }
        if let addr = deliveryModel.location?.address {
            return desStr + " at " + addr
        }
        return desStr
    }

    func getImageUrl() -> URL? {
        return URL(string: deliveryModel.imageUrl ?? "")
    }

    func getLatitude() -> Double {
        return deliveryModel.location?.lat ?? 0.0
    }

    func getLongitude() -> Double {
        return deliveryModel.location?.lng ?? 0.0
    }

    func getAddress() -> String {
        return deliveryModel.location?.address ?? ""
    }
}
