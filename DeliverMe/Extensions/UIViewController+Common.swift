//
//  UIViewController+Common.swift
//  DeliverMe
//
//  Created by Deepak kumar on 04/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title: String = "", message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: StringConstants.okTitle, style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true)
    }
}
