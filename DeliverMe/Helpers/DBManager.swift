//
//  DBManager.swift
//  DeliverMe
//
//  Created by Deepak kumar on 07/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
import CoreData

protocol DBManagerProtocol {
    typealias ResponseBlock = (_ response: [DeliveryModel]?) -> Void
    typealias ErrorBlock = (_ error: String?) -> Void

    func saveDeliveries(deliveries: [DeliveryModel])
    func getDeliveries(offset: Int, limit: Int, completed:@escaping ResponseBlock, failed: @escaping ErrorBlock)
    func deleteDeliveries()
    func isCacheAvailable() -> Bool
    func getAllDeliveries() -> [Delivery]
}

class DBManager: NSObject, DBManagerProtocol {
    static let sharedInstance = DBManager()
    var managedContext: NSManagedObjectContext?

    override private init() {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }

        self.managedContext = appDelegate.persistentContainer.viewContext
    }

    func saveDeliveries(deliveries: [DeliveryModel]) {
        guard let managedContext = managedContext  else {
            return
        }
        let deliveryEntity = NSEntityDescription.entity(forEntityName: CoreDataEntities.delivery,
                                       in: managedContext)!
        let locationEntity = NSEntityDescription.entity(forEntityName: CoreDataEntities.location,
                                                        in: managedContext)!

        for deliveryToSave in deliveries {
            let delivery = NSManagedObject(entity: deliveryEntity,
                                           insertInto: managedContext)
            delivery.setValue(deliveryToSave.id, forKeyPath: "id")
            delivery.setValue(deliveryToSave.description, forKeyPath: "desc")
            delivery.setValue(deliveryToSave.imageUrl, forKeyPath: "imageUrl")

            let location = NSManagedObject(entity: locationEntity,
                                        insertInto: managedContext)
            location.setValue(deliveryToSave.location?.lat, forKeyPath: "lat")
            location.setValue(deliveryToSave.location?.lng, forKeyPath: "lng")
            location.setValue(deliveryToSave.location?.address, forKeyPath: "address")

            delivery.setValue(location, forKeyPath: "deliveryLocation")         //To save location under delivery for 1 to 1 mapping
        }

        do {
            try managedContext.save()
        } catch _ as NSError {
            //print("Could not save. \(error), \(error.userInfo)")
        }
    }

    func getDeliveries(offset: Int, limit: Int, completed:@escaping ResponseBlock, failed: @escaping ErrorBlock) {
        //var deliveries = [Delivery]()
        var deliveryModels = [DeliveryModel]()

        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: CoreDataEntities.delivery)
        fetchRequest.fetchOffset = offset
        fetchRequest.fetchLimit = limit

        do {
            if let deliveries = try managedContext!.fetch(fetchRequest) as? [Delivery] {
                for delivery in deliveries {
                    let location = LocationModel(lat: delivery.deliveryLocation?.lat ?? 0.0, lng: delivery.deliveryLocation?.lng ?? 0.0, address: delivery.deliveryLocation?.address)
                    let deliveryModel = DeliveryModel(id: Int(delivery.id), description: delivery.desc, imageUrl: delivery.imageUrl, location: location)

                    deliveryModels.append(deliveryModel)
                }
            }

            completed(deliveryModels)
        } catch let error as NSError {
            failed(error.description)
        }
    }

    func deleteDeliveries() {
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: CoreDataEntities.delivery)

        do {
            let list = try managedContext!.fetch(fetchRequest)
            for delivery in list {
                managedContext?.delete(delivery)
            }
        } catch _ as NSError {

        }

        do {
            try managedContext!.save()
        } catch _ as NSError {

        }
    }

    func isCacheAvailable() -> Bool {
        return !getAllDeliveries().isEmpty
    }

    func getAllDeliveries() -> [Delivery] {
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: CoreDataEntities.delivery)
        var list = [Delivery]()
        do {
            if let newlist = try managedContext!.fetch(fetchRequest) as? [Delivery] {
                list = newlist
            }
        } catch _ as NSError {
        }

        return list
    }
}
