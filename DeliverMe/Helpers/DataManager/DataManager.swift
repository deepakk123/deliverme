//
//  DataManager.swift
//  DeliverMe
//
//  Created by Deepak kumar on 30/09/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit

class DataManager: DataManagerProtocol {
    typealias ResultBlock = (_ response: [Any]?) -> Void
    typealias ErrorBlock = (_ error: String?) -> Void

    var completionHandler: ResultBlock!
    var failHandler: ErrorBlock!

    var reachabilityManager: ReachabilityProtocol = ReachabilityManager.sharedInstance
    var apiManager: APIManagerProtocol = ApiManager()
    var dbManager: DBManagerProtocol = DBManager.sharedInstance

    func getParamsWith(offset: Int, limit: Int) -> [String: Any] {
        return [RequestKeys.offset: String(offset), RequestKeys.limit: String(limit)]
    }

    func getDeliveriesList(offset: Int, limit: Int, completed:@escaping ResultBlock, failed:@escaping ErrorBlock) {
        completionHandler = completed
        failHandler = failed

        if reachabilityManager.isNetworkReachable() {        //If network is available
            apiManager.requestHTTPGET(url: ApiUrl.baseUrl + RequestKeys.deliveryEndPoint, parameters: getParamsWith(offset: offset, limit: limit), finished: {[weak self] (response) in

                if let data = response, let weakSelf = self {
                    do {
                        var deliveries = [DeliveryModel]()
                        if let data = data as? Data {
                            let decoder = JSONDecoder()
                            deliveries = try decoder.decode([DeliveryModel].self, from: data)
                        } else if data is [DeliveryModel] {
                            deliveries = (data as? [DeliveryModel]) ?? []
                        }

                        //Clean local db on refresh new list
                        if offset == 0 {
                            weakSelf.dbManager.deleteDeliveries()
                        }
                        weakSelf.dbManager.saveDeliveries(deliveries: deliveries)    //Save list to db

                        weakSelf.completionHandler(deliveries)
                    } catch {
                        weakSelf.completionHandler([])
                    }
                }

            }, failed: {[weak self] (error) in
                self?.getDeliveriesFromCache(offset: offset, limit: limit, error: error)
            })
        } else if dbManager.isCacheAvailable() {        //Will cache data from local db if available
            getDeliveriesFromCache(offset: offset, limit: limit)
        }
    }

    func getDeliveriesFromCache(offset: Int, limit: Int, error: String? = nil) {
        dbManager.getDeliveries(offset: offset, limit: limit, completed: {[weak self] (savedList) in
            self?.completionHandler(savedList)
        }, failed: {[weak self] (dberror) in
            self?.failHandler((error != nil) ? error : dberror)
        })
    }
}
