//
//  ReachabilityManager.swift
//  DeliverMe
//
//  Created by Deepak kumar on 30/09/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
import Reachability

protocol ReachabilityProtocol {
    func isNetworkReachable() -> Bool
}

class ReachabilityManager: ReachabilityProtocol {
    static var sharedInstance = ReachabilityManager()

    func isNetworkReachable() -> Bool {
        return !(Reachability()?.connection == Reachability.Connection.none)
    }
}
