//
//  ApiManager.swift
//  DeliverMe
//
//  Created by Deepak kumar on 30/09/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
import Alamofire.Swift

protocol APIManagerProtocol {
    typealias ResponseBlock = (_ response: Any?) -> Void
    typealias ErrorBlock = (_ error: String?) -> Void
    func requestHTTPGET(url: String, parameters: [String: Any]?, finished:@escaping ResponseBlock, failed:@escaping ErrorBlock)
}

class ApiManager: APIManagerProtocol {
    typealias ResponseBlock = (_ response: Any?) -> Void
    typealias ErrorBlock = (_ error: String?) -> Void

    /**
     Make a HTTP GET request and returns with either response or failure
     - Parameter url Request URL
     - Parameter parameters Request Parameters
     */
    func requestHTTPGET(url: String, parameters: [String: Any]?, finished:@escaping ResponseBlock, failed:@escaping ErrorBlock) {
        // HTTP Get request

        _ = AF.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response: AFDataResponse<Any>) in

            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    if let data = response.data {
                        finished(data)
                    }
                case .failure:
                    if let error = response.error {
                        failed(error.localizedDescription)
                    }
                }
            }
        }
    }
}
