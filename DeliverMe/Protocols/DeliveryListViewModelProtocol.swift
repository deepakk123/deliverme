//
//  DeliveryListViewModelProtocol.swift
//  DeliverMe
//
//  Created by Deepak kumar on 30/09/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation

typealias CompletionClosure = (() -> Void)
typealias FailedClosure = ((_ error: String?) -> Void)
typealias LoadMoreClosure = (() -> Void)

protocol DeliveryListViewModelProtocol {
    var refreshHandler: CompletionClosure? { get set }
    var failedHandler: FailedClosure? {get set}
    var loadMoreHandler: LoadMoreClosure? {get set}

    var deliveries: [DeliveryModel] {get set}

    func getDeliveryList(pullToRefresh: Bool)
    func getDeliveryListNextPage()
    func reloadListWithRefresh()

    func checkForFetchNextPage(_ index: Int)
    func getDeliveiesCount() -> Int
    func getItemAtIndex(_ index: Int) -> DeliveryModel?

    func getDeliveryText(forIndex: Int) -> String
    func getDeliveryImageUrl(forIndex index: Int) -> URL?

}
