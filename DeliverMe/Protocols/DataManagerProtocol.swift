//
//  DataManagerProtocol.swift
//  DeliverMe
//
//  Created by Deepak kumar on 09/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation

protocol DataManagerProtocol {
    typealias ResultBlock = (_ response: [Any]?) -> Void
    typealias ErrorBlock = (_ error: String?) -> Void

    func getDeliveriesList(offset: Int, limit: Int, completed:@escaping ResultBlock, failed:@escaping ErrorBlock)
}
