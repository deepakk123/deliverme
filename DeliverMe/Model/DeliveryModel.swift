//
//  DeliveryModel.swift
//  DeliverMe
//
//  Created by Deepak kumar on 30/09/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation

struct DeliveryModel: Codable {
    let id: Int
    let description: String?
    let imageUrl: String?
    let location: LocationModel?
}

struct LocationModel: Codable {
    let lat: Double
    let lng: Double
    let address: String?
}
