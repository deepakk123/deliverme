//
//  DeliveryListViewModelMock.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 11/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
@testable import DeliverMe

class DeliveryListViewModelMock: NSObject, DeliveryListViewModelProtocol {
    var refreshHandler: CompletionClosure?
    var failedHandler: FailedClosure?
    var loadMoreHandler: LoadMoreClosure?
    var deliveries: [DeliveryModel] = []

    var errorExist = false

    func getDeliveryList(pullToRefresh: Bool = false) {
        guard errorExist else {
            deliveries = JSONProvider.getDeliveries()
            refreshHandler?()
            return
        }
        failedHandler?(TestMessages.serverError)
    }

    func getDeliveryListNextPage() {
        guard errorExist else {
            deliveries += JSONProvider.getDeliveries()
            refreshHandler?()
            return
        }
        failedHandler?(TestMessages.serverError)
    }

    func reloadListWithRefresh() {
        guard errorExist else {
            deliveries = JSONProvider.getDeliveries()
            refreshHandler?()
            return
        }
        failedHandler?(TestMessages.serverError)
    }

    func checkForFetchNextPage(_ index: Int) {
        if index == getDeliveiesCount() - 1 {
            getDeliveryListNextPage()
        }
    }

    func getDeliveiesCount() -> Int {
        return deliveries.count
    }

    func getItemAtIndex(_ index: Int) -> DeliveryModel? {
        guard index < deliveries.count else {
            return nil
        }
        return deliveries[index]
    }

    func getDeliveryText(forIndex: Int) -> String {
        guard forIndex < deliveries.count else {
            return ""
        }
        return getDescriptionText(delivery: deliveries[forIndex])
    }

    func getDescriptionText(delivery: DeliveryModel) -> String {
        guard let desStr = delivery.description else {
            return ""
        }
        if let addr = delivery.location?.address {
            return desStr + " at " + addr
        }
        return desStr
    }

    func getDeliveryImageUrl(forIndex index: Int) -> URL? {
        guard index < deliveries.count else {
            return nil
        }
        return URL(string: deliveries[index].location?.address ?? "")
    }

}
