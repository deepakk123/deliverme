//
//  ApiManagerMock.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 10/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
@testable import DeliverMe

enum DeliveryListResponseType {
    case deliveryList
    case serverError

    func handleRequest(completion:@escaping APIManagerProtocol.ResponseBlock, failure:@escaping APIManagerProtocol.ErrorBlock) {
        switch self {
        case .deliveryList:
            completion(JSONProvider.getDeliveries())
        case .serverError:
            failure(TestMessages.serverError)
        }
    }
}

class ApiManagerMock: APIManagerProtocol {
    var responseType: DeliveryListResponseType!

    init(responseType: DeliveryListResponseType) {
        self.responseType = responseType
    }

    func requestHTTPGET(url: String, parameters: [String: Any]?, finished: @escaping ResponseBlock, failed: @escaping ErrorBlock) {
        responseType.handleRequest(completion: finished, failure: failed)
    }
}
