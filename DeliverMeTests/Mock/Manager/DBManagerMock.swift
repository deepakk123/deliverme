//
//  DBManagerMock.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 10/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
import CoreData
@testable import DeliverMe

enum DBResponseType {
    case dbDeliveryList
    case dbError

    func handleRequest(completed: @escaping DBManagerProtocol.ResponseBlock, failed:@escaping  DBManagerProtocol.ErrorBlock) {
        switch self {
        case .dbDeliveryList:
            completed(JSONProvider.getDeliveries())
        case .dbError:
            failed(TestMessages.dbError)
        }
    }
}

class DBManagerMock: DBManagerProtocol {
    var managedObjContext: NSManagedObjectContext?
    var dbResponseType: DBResponseType!
    let deliveryEntity = CoreDataEntities.delivery
    let locationEntity = CoreDataEntities.location

    init(dbResponseType: DBResponseType) {
        //Create persistent store for in memory use
        let mockManagedObjModel = NSManagedObjectModel.mergedModel(from: nil)
        let mockStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: mockManagedObjModel!)
        _ = try? mockStoreCoordinator.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
        self.managedObjContext = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
        self.managedObjContext?.persistentStoreCoordinator = mockStoreCoordinator

        self.dbResponseType = dbResponseType
    }

    func saveDeliveries(deliveries: [DeliveryModel]) {
        guard let managedContext = managedObjContext  else {
            return
        }
        let deliveryEntity = NSEntityDescription.entity(forEntityName: self.deliveryEntity,
                                                        in: managedContext)!
        let locationEntity = NSEntityDescription.entity(forEntityName: self.locationEntity,
                                                        in: managedContext)!

        for deliveryToSave in deliveries {
            let delivery = NSManagedObject(entity: deliveryEntity,
                                           insertInto: managedContext)
            delivery.setValue(deliveryToSave.id, forKeyPath: "id")
            delivery.setValue(deliveryToSave.description, forKeyPath: "desc")
            delivery.setValue(deliveryToSave.imageUrl, forKeyPath: "imageUrl")

            let location = NSManagedObject(entity: locationEntity,
                                           insertInto: managedContext)
            location.setValue(deliveryToSave.location?.lat, forKeyPath: "lat")
            location.setValue(deliveryToSave.location?.lng, forKeyPath: "lng")
            location.setValue(deliveryToSave.location?.address, forKeyPath: "address")

            delivery.setValue(location, forKeyPath: "deliveryLocation")         //To save location under delivery for 1 to 1 mapping
        }

        do {
            try managedContext.save()
        } catch _ as NSError {
            //print("Could not save. \(error), \(error.userInfo)")
        }
    }

    func getDeliveries(offset: Int, limit: Int, completed: @escaping ResponseBlock, failed: @escaping ErrorBlock) {
        dbResponseType.handleRequest(completed: completed, failed: failed)
    }

    func deleteDeliveries() {
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: deliveryEntity)

        do {
            let list = try managedObjContext!.fetch(fetchRequest)
            for delivery in list {
                managedObjContext?.delete(delivery)
            }
        } catch _ as NSError {

        }

        do {
            try managedObjContext!.save()
        } catch _ as NSError {

        }
    }

    func isCacheAvailable() -> Bool {
        return getAllDeliveries().isEmpty
    }

    func getAllDeliveries() -> [Delivery] {
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: CoreDataEntities.delivery)

        var list = [Delivery]()
        do {
            if let newlist = try managedObjContext!.fetch(fetchRequest) as? [Delivery] {
                list = newlist
            }
        } catch _ as NSError {
        }

        return list
    }
}
