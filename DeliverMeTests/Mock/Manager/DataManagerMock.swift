//
//  DataManagerMock.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 10/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
@testable import DeliverMe

enum DataResponseType {
    case deliveryList
    case noDeliveryList
    case dataError

    func handleRequest(completed: @escaping DataManagerProtocol.ResultBlock, failed: @escaping DataManagerProtocol.ErrorBlock) {
        switch self {
        case .deliveryList:
            completed(JSONProvider.getDeliveries())
        case .noDeliveryList:
            completed([])
        case .dataError:
            failed(TestMessages.serverError)
        }
    }
}

class DataManagerMock: DataManagerProtocol {
    var responseType: DataResponseType!

    init(responseType: DataResponseType) {
        self.responseType = responseType
    }

    func getDeliveriesList(offset: Int, limit: Int, completed: @escaping ResultBlock, failed: @escaping ErrorBlock) {
        responseType.handleRequest(completed: completed, failed: failed)
    }
}
