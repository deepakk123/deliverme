//
//  ReachabilityManagerMock.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 09/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
@testable import DeliverMe

class ReachabilityManagerMock: ReachabilityProtocol {
    var isReachable: Bool!

    init(isReachable: Bool) {
        self.isReachable = isReachable
    }

    func isNetworkReachable() -> Bool {
        return isReachable
    }
}
