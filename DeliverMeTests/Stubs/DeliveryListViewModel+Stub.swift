//
//  DeliveryListViewModel+Stub.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 10/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
@testable import DeliverMe

extension DeliveryListViewModel {
    static func stub(reachabilityManager: ReachabilityManagerMock = ReachabilityManagerMock(isReachable: true)) -> DeliveryListViewModel {
        let deliveryListViewModel = DeliveryListViewModel()
        deliveryListViewModel.reachabilityManager = reachabilityManager
        return deliveryListViewModel
    }
}
