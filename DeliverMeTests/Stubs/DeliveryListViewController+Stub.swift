//
//  DeliveryListViewController+Stub.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 11/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
import UIKit
@testable import DeliverMe

extension DeliveryListViewController {
    static func stub(deliveryListViewModel: DeliveryListViewModelMock) -> DeliveryListViewController {
        let deliveryListVC = DeliveryListViewController()
        deliveryListVC.deliverylistViewModel = deliveryListViewModel

        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        let navController = UINavigationController(rootViewController: deliveryListVC)
        window.rootViewController = navController

        _ = deliveryListVC.view
        return deliveryListVC
    }
}
