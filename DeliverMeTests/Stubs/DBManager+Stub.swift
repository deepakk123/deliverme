//
//  DBManager+Stub.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 10/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
import CoreData
@testable import DeliverMe

extension DBManager {
    static func stub() -> DBManagerProtocol {
        let dbManager = DBManager.sharedInstance

        //Create persistent store for in memory use
        let mockManagedObjModel = NSManagedObjectModel.mergedModel(from: nil)
        let mockStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: mockManagedObjModel!)
        _ = try? mockStoreCoordinator.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
        let managedObjContext = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
        managedObjContext.persistentStoreCoordinator = mockStoreCoordinator

        dbManager.managedContext = managedObjContext
        return dbManager
    }
}
