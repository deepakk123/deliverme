//
//  DeliveryDetailViewController+Stub.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 11/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
@testable import DeliverMe

extension DeliveryDetailsViewController {
    static func stub() -> DeliveryDetailsViewController {
        let deliveryDetailViewModel = DeliveryDetailViewModel(delivery: DeliveryModel(id: 0, description: "Deliver Car To John", imageUrl: "www.image.com", location: LocationModel(lat: 28.451792, lng: 76.999438, address: "Gurugram")))
        let deliveryDetailVC = DeliveryDetailsViewController(detailViewModel: deliveryDetailViewModel)

        return deliveryDetailVC
    }
}
