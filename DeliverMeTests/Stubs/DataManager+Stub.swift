//
//  DataManager+Stub.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 10/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
@testable import DeliverMe

extension DataManager {
    static func stub(reachabilityManager: ReachabilityManagerMock = ReachabilityManagerMock(isReachable: true)) -> DataManagerProtocol {

        let dataManager = DataManager()
        dataManager.reachabilityManager = reachabilityManager
        return dataManager
    }
}
