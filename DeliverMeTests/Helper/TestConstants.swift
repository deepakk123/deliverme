//
//  TestConstants.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 10/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation

struct TestMessages {
    static let serverError = "Http Error"
    static let dbError = "Database Error"
}
