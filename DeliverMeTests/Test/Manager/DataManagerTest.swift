//
//  DataManagerTest.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 09/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import XCTest
@testable import DeliverMe

class DataManagerTest: XCTestCase {
    var dataManager: DataManagerProtocol!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        dataManager = DataManager.stub()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testServerReturnList() {
        let apiManagerMock = ApiManagerMock(responseType: .deliveryList)
        (dataManager as? DataManager)?.apiManager = apiManagerMock

        dataManager.getDeliveriesList(offset: 0, limit: 10, completed: { (response) in
            XCTAssert(!(response!.isEmpty), "data not found")
        }, failed: { (_) in

        })
    }

    func testServerReturnErrorAndDbContainsData() {
        let apiManagerMock = ApiManagerMock.init(responseType: .serverError)
        (dataManager as? DataManager)?.apiManager = apiManagerMock
        let dbManagerMock = DBManagerMock.init(dbResponseType: .dbDeliveryList)
        (dataManager as? DataManager)?.dbManager = dbManagerMock

        dataManager.getDeliveriesList(offset: 0, limit: 10, completed: { (response) in
            XCTAssert(!(response!.isEmpty), "data not found")
        }, failed: { (_) in

        })
    }

    func testServerReturnErrorAndDbContainsNoData() {
        let apiManagerMock = ApiManagerMock.init(responseType: .serverError)
        (dataManager as? DataManager)?.apiManager = apiManagerMock
        let dbManagerMock = DBManagerMock.init(dbResponseType: .dbError)
        (dataManager as? DataManager)?.dbManager = dbManagerMock

        dataManager.getDeliveriesList(offset: 0, limit: 10, completed: { (response) in
            XCTAssertNil(response, "No data")
        }, failed: { (error) in
            XCTAssert(error != nil, "No error found")
        })
    }

    func testNoInternetAndDbContainsData() {
        dataManager = DataManager.stub(reachabilityManager: ReachabilityManagerMock(isReachable: false))
        let dbManagerMock = DBManagerMock.init(dbResponseType: .dbDeliveryList)
        (dataManager as? DataManager)?.dbManager = dbManagerMock

        dataManager.getDeliveriesList(offset: 0, limit: 10, completed: { (response) in
            XCTAssert(!(response!.isEmpty), "data not found")
        }, failed: { (_) in

        })
    }

    func testNoInternetAndDbContainsNoData() {
        dataManager = DataManager.stub(reachabilityManager: ReachabilityManagerMock(isReachable: false))
        let dbManagerMock = DBManagerMock.init(dbResponseType: .dbError)
        (dataManager as? DataManager)?.dbManager = dbManagerMock

        dataManager.getDeliveriesList(offset: 0, limit: 10, completed: { (_) in

        }, failed: { (error) in
            XCTAssert(error != nil, "No error found")
        })
    }

}
