//
//  DBManagerTest.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 10/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import XCTest
@testable import DeliverMe

class DBManagerTest: XCTestCase {
    var dbManager: DBManagerProtocol! = DBManager.stub()
    let validDeliveriesJson = "validDeliveryList"
    let invalidDeliveriesJson = "invalidDeliveryList"

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSavingDeliveries() {
        dbManager.deleteDeliveries()

        do {
            let decoder = JSONDecoder()
            let deliveries = try decoder.decode([DeliveryModel].self, from: JSONProvider.jsonFileToData(jsonName: validDeliveriesJson)!)
            dbManager.saveDeliveries(deliveries: deliveries)
        } catch {

        }

        XCTAssert(dbManager.getAllDeliveries().count == 20, "Error in saving data")
        XCTAssertTrue(dbManager.isCacheAvailable(), "local cache not available")

        dbManager.getDeliveries(offset: 0, limit: 8, completed: { (deliveries) in
            XCTAssert(deliveries?.count == 8, "Not getting data with correct limit")
        }, failed: { (_) in

        })
    }

    func testSavingInvalidDeliveries() {
        dbManager.deleteDeliveries()
        do {
            let decoder = JSONDecoder()
            let deliveries = try decoder.decode([DeliveryModel].self, from: JSONProvider.jsonFileToData(jsonName: invalidDeliveriesJson)!)
            dbManager.saveDeliveries(deliveries: deliveries)
        } catch {

        }

        XCTAssertTrue(dbManager.isCacheAvailable(), "local cache not available")
    }

    func testDeleteCache() {
        dbManager.deleteDeliveries()

        XCTAssert(dbManager.getAllDeliveries().isEmpty, "Error in deleting data")
        XCTAssertTrue(!dbManager.isCacheAvailable(), "No local cache available")
    }
}
