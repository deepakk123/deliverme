//
//  DeliveryDetailViewModelTest.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 11/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import XCTest
@testable import DeliverMe

class DeliveryDetailViewModelTest: XCTestCase {
    var deliveryDetailViewModel: DeliveryDetailViewModel!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testDetailViewController() {
        deliveryDetailViewModel = DeliveryDetailViewModel(delivery: DeliveryModel(id: 0, description: "Deliver Car To John", imageUrl: "www.image.com", location: LocationModel(lat: 28.451792, lng: 76.999438, address: "Gurugram")))

        XCTAssert(deliveryDetailViewModel.getDeliveryText() == "Deliver Car To John at Gurugram", "Text not correct")

        deliveryDetailViewModel = DeliveryDetailViewModel(delivery: DeliveryModel(id: 0, description: "Deliver Car To John", imageUrl: "www.image.com", location: LocationModel(lat: 28.451792, lng: 76.999438, address: nil)))

        XCTAssert(deliveryDetailViewModel.getDeliveryText() == "Deliver Car To John", "Text not correct")

        deliveryDetailViewModel = DeliveryDetailViewModel(delivery: DeliveryModel(id: 0, description: "Deliver Car To John", imageUrl: nil, location: LocationModel(lat: 28.451792, lng: 76.999438, address: "Gurugram")))

        XCTAssert(deliveryDetailViewModel.getImageUrl() == nil, "url not correct")

        deliveryDetailViewModel = DeliveryDetailViewModel(delivery: DeliveryModel(id: 0, description: "Deliver Car To John", imageUrl: "www.image.com", location: nil))

        XCTAssert(deliveryDetailViewModel.getLatitude() == 0.0, "location lat not correct")
        XCTAssert(deliveryDetailViewModel.getLongitude() == 0.0, "location lng not correct")
        XCTAssert(deliveryDetailViewModel.getAddress() == "", "location address not correct")
    }

}
