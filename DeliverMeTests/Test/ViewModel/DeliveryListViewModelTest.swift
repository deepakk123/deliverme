//
//  DeliveryListViewModelTest.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 10/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import XCTest
@testable import DeliverMe

class DeliveryListViewModelTest: XCTestCase {
    var deliveryListViewModel: DeliveryListViewModel!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        deliveryListViewModel = DeliveryListViewModel.stub()
        deliveryListViewModel.dataManager = DataManagerMock(responseType: .deliveryList)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testGettingDeliveries() {
        //Check after send request to server
        deliveryListViewModel.getDeliveryList(pullToRefresh: false)
        XCTAssert(deliveryListViewModel.getDeliveiesCount() == 20, "Error in getting data")
        XCTAssert(!deliveryListViewModel.isApiLoading, "Api request in progress flag false")

        //Check if api is in progress and send request again.
        deliveryListViewModel.isApiLoading = true
        deliveryListViewModel.getDeliveryList(pullToRefresh: false)
        XCTAssert(deliveryListViewModel.getDeliveiesCount() == 20, "Request hit again")

        let txt = deliveryListViewModel.getDeliveryText(forIndex: 0)
        let url = deliveryListViewModel.getDeliveryImageUrl(forIndex: 0)
        XCTAssert(txt == "Deliver documents to Andrio at Kowloon Tong", "Description not correct")
        XCTAssert(url == URL(string: "https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-5.jpeg"), "Image url not correct")

        let txtOutRange = deliveryListViewModel.getDeliveryText(forIndex: 20)
        let urlOutRange = deliveryListViewModel.getDeliveryImageUrl(forIndex: 20)
        XCTAssert(txtOutRange.isEmpty, "desc not correct")
        XCTAssert(urlOutRange == nil, "url not correct")
    }

    func testGettingDeliveriesWithNextPage() {
        //check for load more for next page
        deliveryListViewModel.getDeliveryList(pullToRefresh: false)
        deliveryListViewModel.shouldGetNextPage = true
        deliveryListViewModel.getDeliveryList(pullToRefresh: false)
        XCTAssert(deliveryListViewModel.getDeliveiesCount() == 40, "Error in getting data")

        //Check for pull to refresh
        deliveryListViewModel.reloadListWithRefresh()
        XCTAssert(deliveryListViewModel.getDeliveiesCount() == 20, "Error in getting data")
        XCTAssertTrue(deliveryListViewModel.shouldGetNextPage, "Next page not avaialble")
    }

    func testGettingDeliveriesAndNextPageNotAvailable() {
        deliveryListViewModel.getDeliveryList(pullToRefresh: false)

        deliveryListViewModel.dataManager = DataManagerMock(responseType: .noDeliveryList)
        deliveryListViewModel.getDeliveryList(pullToRefresh: false)
        XCTAssert(deliveryListViewModel.getDeliveiesCount() == 20, "Error in getting data")
        XCTAssertTrue(!deliveryListViewModel.shouldGetNextPage, "Next page avaialble")
    }

    func testGettingDeliveriesAndNoInternet() {
        deliveryListViewModel = DeliveryListViewModel.stub(reachabilityManager: ReachabilityManagerMock(isReachable: false))
        deliveryListViewModel.dbManager = DBManagerMock(dbResponseType: .dbError)
        deliveryListViewModel.getDeliveryList(pullToRefresh: false)

        XCTAssert(deliveryListViewModel.getDeliveiesCount() == 0, "Error in getting data")
    }

}
