//
//  DeliveryDetailViewControllerTest.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 11/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import XCTest
@testable import DeliverMe

class DeliveryDetailViewControllerTest: XCTestCase {
    var deliveryDetailVC: DeliveryDetailsViewController!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        deliveryDetailVC = DeliveryDetailsViewController.stub()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        deliveryDetailVC = nil
        super.tearDown()
    }

    func testViewControllerSetupUI() {
        deliveryDetailVC.setupUI()

        XCTAssert(deliveryDetailVC.mapView != nil, "MapView could not be created")
        XCTAssert(deliveryDetailVC.deliveryDetailsView != nil, "Delivery Detail view could not be created")
        XCTAssert(deliveryDetailVC.deliveryPin != nil, "Delivery pin could not be added")

        XCTAssert(deliveryDetailVC.mapViewModel.getDeliveryText() == deliveryDetailVC.deliveryDetailsView.deliveryLabel.text, "Description not correct")
        XCTAssert(deliveryDetailVC.deliveryPin.title == deliveryDetailVC.mapViewModel.getAddress(), "Pin Title not correct")
    }

}
