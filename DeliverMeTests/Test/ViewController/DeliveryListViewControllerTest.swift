//
//  DeliveryListViewControllerTest.swift
//  DeliverMeTests
//
//  Created by Deepak kumar on 09/10/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import XCTest
@testable import DeliverMe

class DeliveryListViewControllerTest: XCTestCase {
    var deliveryListVC: DeliveryListViewController!
    let timeOutInterval = 2.0

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        let deliveryListViewModelMock = DeliveryListViewModelMock()
        deliveryListVC = DeliveryListViewController.stub(deliveryListViewModel: deliveryListViewModelMock)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        deliveryListVC = nil
        super.tearDown()
    }

    func getDeliveries() {
        deliveryListVC.deliverylistViewModel.getDeliveryList(pullToRefresh: false)
    }

    func testViewControllerSetupUI() {
        XCTAssert(deliveryListVC.tableView != nil, "TableView could not be created")
        XCTAssert(deliveryListVC.activityLoader != nil, "Activity view could not be created")
    }

    func testTableAfterGetDeliveries() {
        self.getDeliveries()

        XCTAssert(deliveryListVC.tableView(deliveryListVC.tableView, numberOfRowsInSection: 0) > 0, "No Delivery list found")
    }

    func testWhenServerReturnError() {
        let deliveryListViewModelMock = DeliveryListViewModelMock()
        deliveryListViewModelMock.errorExist = true
        deliveryListVC = DeliveryListViewController.stub(deliveryListViewModel: deliveryListViewModelMock)

        self.getDeliveries()

        let expectation = XCTestExpectation(description: "testAlert")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            XCTAssertTrue(UIApplication.shared.keyWindow?.rootViewController?.presentedViewController is UIAlertController)
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: timeOutInterval)
    }

    func testTableViewDidSelectRow() {
        //self.getDeliveries()

        self.deliveryListVC.tableView(self.deliveryListVC.tableView, didSelectRowAt: IndexPath(row: 1, section: 0))

        let expectation = XCTestExpectation(description: "testNavigateMapScreen")

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {[weak self] in
            if let topVC = self?.deliveryListVC.navigationController?.topViewController {
                if topVC is DeliveryDetailsViewController && (topVC as? DeliveryDetailsViewController)!.mapViewModel.getDeliveryText() == "Deliver food to Eric at Mong Kok" {

                } else {
                    XCTFail("Error is showing map screen")
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeOutInterval)
    }

    func testWillDisplayMethod() {
        self.getDeliveries()

        deliveryListVC.tableView(deliveryListVC.tableView, willDisplay: UITableViewCell(), forRowAt: IndexPath(row: deliveryListVC.deliverylistViewModel.deliveries.count - 1, section: 0))

        XCTAssert(deliveryListVC.deliverylistViewModel.getDeliveiesCount() == 40, "Could not get next page")
    }

    func testBottomLoader() {
        deliveryListVC.showBottomLoader()
        XCTAssertTrue(deliveryListVC.activityLoader.isAnimating)

        deliveryListVC.hideBottomLoader()
        XCTAssertTrue(!deliveryListVC.activityLoader.isAnimating)
    }

}
